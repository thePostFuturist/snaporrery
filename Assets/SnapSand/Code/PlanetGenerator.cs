﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetGenerator : MonoBehaviour {

	[SerializeField]
	GameObject planet_model;

	[SerializeField]
	List<PlanetInfo> planet_info_list = new List<PlanetInfo>();

	List<GameObject> planet_models = new List<GameObject>();


	void Start()
	{
		GeneratePlanets();
	}

	void GeneratePlanets()
	{
		foreach (PlanetInfo planet_info in planet_info_list)
		{
			GameObject new_planet = GameObject.Instantiate(planet_model);
			SetPlanetProperties(planet_info, new_planet);
		}
	}

	void SetPlanetProperties(PlanetInfo planet_info, GameObject new_planet)
	{
		PlanetMechanics planet_mechanics = new_planet.transform.find<PlanetMechanics>("Planet");
		planet_mechanics.planet_info = planet_info;
	}

}
