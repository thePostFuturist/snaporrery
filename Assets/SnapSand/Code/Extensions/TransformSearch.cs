﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class TransformSearch
{

	public static T find <T> (this Transform m_transform, string m_name) where T: Component  {
	
		T found_component = null;
		T[] found_components = m_transform.GetComponentsInChildren<T> (true);

		foreach (T comp in found_components) {
			if (comp.name == m_name) {
				found_component = comp;
			}
		}
		
		if (found_component == null)
			Debug.LogError(m_name + " NOT FOUND as child in " + m_transform.name);
		

		return found_component;

	}
	
	public static T EntityComponent <T> (this GameObject _game_object) where T: Component
	{
		T entity_component = _game_object.GetComponent<T>();
		
		if (entity_component == null)
			Debug.LogError(_game_object.name + " entity DOES NOT implement expected component ");
		
		return entity_component;
	}

    public static T EntityComponent<T>(this GameObject _game_object, bool should_add) where T : Component
    {
        T entity_component = _game_object.GetComponent<T>();

        if (entity_component == null)
        {
            Debug.LogError(_game_object.name + " entity DOES NOT implement expected component, adding " + entity_component);
            _game_object.AddComponent<T>();
        }

        return entity_component;
    }

}