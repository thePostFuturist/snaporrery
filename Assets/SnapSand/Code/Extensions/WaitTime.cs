﻿using UnityEngine;
using System.Collections;

public class WaitTime {

	public static IEnumerator Wait (System.Action m_callback, float m_time)
	{
		yield return new WaitForSeconds (m_time);
		m_callback ();
	}

	public static IEnumerator Wait (float m_time, System.Action m_callback)
	{
		yield return new WaitForSeconds (m_time);
		m_callback ();
	}

	public static IEnumerator WaitFrame (System.Action m_callback)
	{
		yield return new WaitForEndOfFrame ();
		m_callback ();
	}

}
