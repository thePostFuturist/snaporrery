﻿using UnityEngine;


public static class RemapExtension{

	public static float Remap (this float value, float from1, float to1, float from2, float to2) {
		return (value - from1) / (to1 - from1) * (to2 - from2) + from2;

	}

    public static Vector2 GeoCoordinatesToUV(this Vector2 value)
    {
        float lat = value.x.Remap(-180f, 180f, 0, 1);
        float lon = value.y.Remap(-90, 90, 0, 1);
        return new Vector2(lat, lon);
    }
}
