﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// Usage:
///   var rbdy = gameObject.FirstAncestorOfType<RigidBody>();
/// </summary>
public static class AncestorFind 
{

	public static T FirstAncestorOfType<T>(this GameObject gameObject) where T : Component
	{
		var t = gameObject.transform.parent;
		T component = null;
		while (t != null && (component = t.GetComponent<T>()) == null)
		{
			t = t.parent;
		}
		return component;
	}

	public static T LastAncestorOfType<T>(this GameObject gameObject) where T : Component
	{
		var t = gameObject.transform.parent;
		T component = null;
		while (t != null)
		{
			var c = t.gameObject.GetComponent<T>();
			if (c != null)
			{
				component = c;
			}
			t = t.parent;
		}
		return component;
	}


}