﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class FloatComparison
{


    public static bool CompareFloats(this float f1, float f2) 
    {
        return Mathf.Abs(f1 - f2) < 0.00001f;
    }


}