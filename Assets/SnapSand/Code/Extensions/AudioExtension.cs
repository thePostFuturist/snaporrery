﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static class AudioExtension {

	public static void PlayClip(this AudioSource audio_source, AudioClip audio_clip)
	{
		audio_source.clip = audio_clip;
		audio_source.Play ();
	}

	public static IEnumerator PlayClip(this AudioSource audio_source, AudioClip audio_clip, Action onComplete)
	{
		audio_source.PlayClip (audio_clip);
        audio_source.loop = false;
		while (audio_source.isPlaying) {
			yield return null;
		}

		onComplete ();
	}
}
