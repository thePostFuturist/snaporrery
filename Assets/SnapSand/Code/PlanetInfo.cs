﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class PlanetInfo {
	public float rotation_speed;
	public float revolution_speed;
	public Vector3 size;

	public Material material;

	public Vector3 distance_from_center;
}
