﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetMechanics : MonoBehaviour {

	[SerializeField]
	public PlanetInfo planet_info;


	void Start()
	{
		SetInfo();
		StartCoroutine(PerformMechanics());
	}

	void SetInfo()
	{
		// set size
		transform.localScale = planet_info.size;

		SetMaterial();

		// position from origin
		transform.localPosition = planet_info.distance_from_center;

		transform.parent.localEulerAngles = Vector3.up * Random.Range(0f, 360f);
	}

	void SetMaterial()
	{
		if (planet_info.material !=null)
			gameObject.GetComponent<Renderer>().material = planet_info.material;
	}

	IEnumerator PerformMechanics()
	{
		while (true)
		{
			PlanetRotate();
			PlanetOrbit();
			yield return null;
		}
	}

	void PlanetRotate()
	{
		transform.Rotate(Vector3.up * Time.deltaTime * planet_info.revolution_speed);
	}

	void PlanetOrbit()
	{
		transform.parent.Rotate(Vector3.up * planet_info.rotation_speed);
	}

}
